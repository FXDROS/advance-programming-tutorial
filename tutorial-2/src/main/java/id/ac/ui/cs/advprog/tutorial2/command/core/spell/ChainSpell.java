package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    ArrayList<Spell> getSpell;
    public ChainSpell (ArrayList<Spell> getSpell){
        this.getSpell = getSpell;
    }

    @Override
    public void cast() {
        for (Spell toCast : getSpell){
            toCast.cast();
        }
    }

    @Override
    public void undo() {
        for (Spell toUndo : getSpell)
            toUndo.undo();
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
