package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        //ToDo: Complete Me
        this.guild = guild;
    }

    //ToDo: Complete Me
    public void update(){
        System.out.println(guild.getQuestType());
        if (!guild.getQuestType().equals("E")) {
            getQuests().add(guild.getQuest());
//            System.out.println("hello");
        }
    }
}
