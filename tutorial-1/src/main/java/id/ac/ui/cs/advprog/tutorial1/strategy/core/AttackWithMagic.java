package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    //ToDo: Complete me
    @Override
    public String attack() {
        return "Avada Kadabra!!!";
    }

    @Override
    public String getType() {
        return "Attack With Magic";
    }
}
