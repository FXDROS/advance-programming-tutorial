package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ElementalTransformationTest {
    private Class<?> elementalClass;

    @BeforeEach
    public void setup() throws Exception {
        elementalClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.ElementalTransformation");
    }

    @Test
    public void testElementalHasEncodeMethod() throws Exception {
        Method translate = elementalClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testElementalEncodesCorrectly() throws Exception {
        String text = "abc";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "def";

        Spell result = new ElementalTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

//    @Test
//    public void testElementalEncodesCorrectlyWithCustomKey() throws Exception {
//        String text = "abc";
//        Codex codex = AlphaCodex.getInstance();
//        Spell spell = new Spell(text, codex);
//        String expected = "def";
//
//        Spell result = new ElementalTransformation("SafiraEmyra").encode(spell);
//        assertEquals(expected, result.getText());
//    }

    @Test
    public void testElementalHasDecodeMethod() throws Exception {
        Method translate = elementalClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testElementalDecodesCorrectly() throws Exception {
        String text = "def";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "abc";

        Spell result = new ElementalTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }

//    @Test
//    public void testElementalDecodesCorrectlyWithCustomKey() throws Exception {
//        String text = "def";
//        Codex codex = AlphaCodex.getInstance();
//        Spell spell = new Spell(text, codex);
//        String expected = "abc";
//
//        Spell result = new ElementalTransformation("SafiraEmyra").decode(spell);
//        assertEquals(expected, result.getText());
//    }
}
