package id.ac.ui.cs.advprog.tutorial3.adapter.core.bow;

public interface Bow {
    String shootArrow(boolean isAimShoot);
    String getName();
    String getHolderName();
    String useState();
}