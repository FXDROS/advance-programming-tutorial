package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;
    @Autowired
    private BowRepository bowRepository;
    @Autowired
    private SpellbookRepository spellbookRepository;
    @Autowired
    private WeaponRepository weaponRepository;
    List<Bow> getAllBow;
    List<Spellbook> getSpellBook;
    List<Weapon> getAllWeapon;
    List<String> getLog;

    // TODO: implement me
    @Override
    public List<Weapon> findAll() {
        getAllBow = bowRepository.findAll();
        getSpellBook = spellbookRepository.findAll();
        getAllWeapon = weaponRepository.findAll();
        for (Bow toAdd : this.getAllBow){
            weaponRepository.save(new BowAdapter(toAdd));
        }
        for (Spellbook toAdd : this.getSpellBook){
            weaponRepository.save(new SpellbookAdapter(toAdd));
        }
        return weaponRepository.findAll();
    }

    // TODO: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon toUse = weaponRepository.findByAlias(weaponName);
        if (attackType == 1) {
            logRepository.addLog(toUse.getHolderName() + " attacked with " + toUse.getName() + " (charge attack): " + toUse.chargedAttack());
        }else if (attackType == 0) {
            logRepository.addLog(toUse.getHolderName() + " attacked with " + toUse.getName() + " (normal attack): " + toUse.normalAttack());
        }
        weaponRepository.save(toUse);
    }

    // TODO: implement me
    @Override
    public List<String> getAllLogs() {
        getLog = logRepository.findAll();
        return this.getLog;
    }
}
