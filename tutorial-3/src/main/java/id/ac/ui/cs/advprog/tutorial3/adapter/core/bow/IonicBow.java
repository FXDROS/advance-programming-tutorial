package id.ac.ui.cs.advprog.tutorial3.adapter.core.bow;

public class IonicBow implements Bow {

    private boolean currentState;
    private String holderName;

    public IonicBow(String holderName) {
        this.holderName = holderName;
        this.currentState = true;
    }

    @Override
    public String shootArrow(boolean isAimShoot) {
        if(this.currentState) {
            return "Arrow reacted with the enemy's protons";
        } else {
            return "Separated one atom from the enemy";
        }
    }

    @Override
    public String getName() {
        return "Ionic Bow";
    }

    @Override
    public String getHolderName() { return holderName; }

    @Override
    public String useState() {
        this.currentState = !this.currentState;
        if (this.currentState) {
            return "Entered aim shot mode";
        } else {
            return "Entered no scope mode";
        }
    }
}