package id.ac.ui.cs.advprog.tutorial3.adapter.core.bow;

public class UranosBow implements Bow {
    private boolean currentState;
    private String holderName;

    public UranosBow(String holderName) {
        this.holderName = holderName;
        this.currentState = true;
    }

    @Override
    public String shootArrow(boolean isAimShoot) {
        if (this.currentState) {
            return "Gaining charge... gaining speed... headshot!";
        } else {
            return "headshot!";
        }
    }

    @Override
    public String getName() {
        return "Uranos Bow";
    }

    @Override
    public String getHolderName() { return holderName; }

    @Override
    public String useState() {
        this.currentState = !this.currentState;
        if (this.currentState) {
            return "Entered aim shot mode";
        } else {
            return "Entered no scope mode";
        }
    }

}