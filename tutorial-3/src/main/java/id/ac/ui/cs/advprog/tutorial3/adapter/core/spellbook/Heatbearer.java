package id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook;


public class Heatbearer implements Spellbook {

    private String holderName;
    private int Counter;

    public Heatbearer(String holderName) {
        this.holderName = holderName;
        this.Counter = 0;
    }

    @Override
    public String smallSpell() {
        Counter = 0;
        return "Enemy scarred";
    }

    @Override
    public String largeSpell() {
        Counter += 1;
        return (Counter % 2 == 1 ? "EXPUULOOOOSHHHIOONNNN!" : "Out of manna");
    }

    @Override
    public String getName() {
        return "Heat Bearer";
    }

    @Override
    public String getHolderName() { return holderName; }
}