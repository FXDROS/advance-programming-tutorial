package id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook;

public class TheWindjedi implements Spellbook {

    private String holderName;
    private int Counter;

    public TheWindjedi(String holderName) {
        this.holderName = holderName;
        this.Counter = 0;
    }

    @Override
    public String smallSpell() {
        Counter = 0;
        return "Small musical attack launched";
    }

    @Override
    public String largeSpell() {
        Counter += 1;
        return (Counter % 2 ==  1 ? "Orchestra-class music attack launched" : "Out of manna");
    }

    @Override
    public String getName() {
        return "The Windjedi";
    }

    @Override
    public String getHolderName() { return holderName; }
}